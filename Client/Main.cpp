#include "Connection.h"

int main()
{
	std::string host;
	std::string port = "4000";
	std::string cert = "CAChain.pem";

	std::cin >> host;

	Connection c(host, port, cert, 30);
	c.work();
}