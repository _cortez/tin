#include "Connection.h"

Connection::Connection(std::string& address, std::string& port, std::string& caCert, int ioTimeout)
	: _ioService(),
	_context(boost::asio::ssl::context::sslv23),
	_ioTimeout(ioTimeout),
	_session(nullptr)
{
	_context.use_certificate_chain_file(caCert);

	boost::asio::ip::tcp::resolver resolver(_ioService);
	boost::asio::ip::tcp::resolver::query query(address, port);
	_endpointIterator = resolver.resolve(query);
}

Connection::~Connection()
{
	_session->stop();
}

void Connection::work()
{
	connect();
	while (_session->isOpen())
	{
		std::string msg;
		int length[1];

		std::cout << ">";
		std::cin >> msg;
		length[0] = (int)msg.size();

		if (msg == "close")
		{
			_session->stop();
			system("pause");
		}

		_session->write(boost::asio::buffer(length));
		_session->write(boost::asio::buffer(&msg[0], msg.size()));

		msg.clear();

		_session->read(boost::asio::buffer(length));
		msg.resize(length[0]);
		_session->read(boost::asio::buffer(&msg[0], msg.size()));

		std::cout << msg << std::endl;
	}
	std::cout << "Connection stopped\n";
}

boost::system::error_code Connection::connect()
{
	boost::system::error_code ec;

	if (!_session)_session = new SSLSession(_ioService, _context, _ioTimeout);

	boost::asio::connect(_session->lowestLayerSocket(), _endpointIterator, ec);

	_session->sslHandshake(boost::asio::ssl::stream_base::handshake_type::client);

	return ec;
}