#pragma once

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <iostream>
#include "../Server/SSLSession.h"

class Connection
{
public:
	Connection(std::string& address, std::string& port, std::string& caCert, int timeout);
	~Connection();
	void work();
private:
	boost::system::error_code connect();

	boost::asio::io_service _ioService;
	boost::asio::ssl::context _context;
	boost::asio::ip::tcp::resolver::iterator _endpointIterator;
	SSLSession* _session;
	int _ioTimeout;
};