#pragma once

#include <iostream>
#include <fstream>
#include <string>

struct Settings
{
	std::string ip;
	int port;
	int timeout;
	int backlogSize;
	std::string cert;
	std::string key;
	std::string dhFile;
	std::string keyPass;

	bool load(std::string fileName);

private:
	bool readParam(std::ifstream& cfg, std::string& param);
};