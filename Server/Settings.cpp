#include "Settings.h"

bool Settings::load(std::string fileName)
{
	std::ifstream cfg;

	cfg.open(fileName, std::ios::in);

	if (cfg.is_open())
	{
		if (!readParam(cfg, ip))return false;
		if (!(cfg >> port >> timeout >> backlogSize))return false;
		if (!readParam(cfg, cert))return false;
		if (!readParam(cfg, key))return false;
		if (!readParam(cfg, dhFile))return false;
		if (!readParam(cfg, keyPass))return false;
	}
	else
	{
		return false;
	}

	cfg.close();

	return true;
}

bool Settings::readParam(std::ifstream& cfg, std::string& param)
{
	do
	{
		if (!std::getline(cfg, param)) return false;
	} 
	while (param == "");

	return true;
}