#pragma once

#include <vector>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <boost/bind.hpp>

typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_tcp_socket;

class SSLSession
{
public:
	SSLSession(boost::asio::io_service& ioService, boost::asio::ssl::context& context, int ioTimeout);
	~SSLSession();
	ssl_tcp_socket::lowest_layer_type& lowestLayerSocket();
	ssl_tcp_socket& sslSocket();
	boost::system::error_code sslHandshake(boost::asio::ssl::stream_base::handshake_type type);
	boost::system::error_code read(boost::asio::mutable_buffers_1& buffer);
	boost::system::error_code write(boost::asio::mutable_buffers_1& buffer);
	boost::system::error_code stop();
	bool isOpen();
private:
	void deadlineStop(boost::asio::deadline_timer* timer);

	boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket;
	boost::asio::deadline_timer _ioDeadline;
	int _ioTimeout;
};