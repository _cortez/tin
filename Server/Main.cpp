#include <iostream>
#include <boost/thread.hpp>
#include "Server.h"

int main(int argc, char* argv[])
{
	Settings settings;
	if (settings.load("cfg.cfg"))
	{
		try
		{
			Server s(settings);
			boost::thread t([&s]()
			{
				s.startAccept();
			});

			std::string msg;
			bool isActive = true;
			do
			{
				std::cout << '>';
				std::cin >> msg;

				if (msg == "status")
				{
					std::cout << "Active connections: " << s.activeConnections() << std::endl;
				}
				else if (msg == "help")
				{
					std::cout << "help - list of commands\n"
						<< "status - number of active connections\n"
						<< "quit - shut down the server\n";
				}
				else if (msg == "quit") isActive = false;
				else
				{
					std::cout << "Unknown command. Type help for list of commands.\n";
				}
			} while (isActive);

			s.stop();
			std::cout << "Shutting down\n";

			t.join();
			std::cout << "Server shut down\n";
			system("pause");
			return 0;
		}
		catch (boost::system::system_error e)
		{
			std::cout << "Server configuration unsuccessful.\n" << e.what() << std::endl;
			system("pause");
			return 0;
		}
	}
	else
	{
		std::cout << "Unable to open config file or file incomplete.\n";
		system("pause");
		return 0;
	}
}