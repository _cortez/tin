#include "SenderLayer.h"

SenderLayer::SenderLayer(boost::asio::io_service& ioService, boost::asio::ssl::context& context, int ioTimeout) :
	_session(ioService, context, ioTimeout)
{
}

boost::system::error_code SenderLayer::connect(boost::asio::ssl::stream_base::handshake_type type)
{
	return _session.sslHandshake(type); //boost::asio::ssl::stream_base::handshake_type::server
}

boost::system::error_code SenderLayer::sendMsg(const Msg* msg)
{
	boost::system::error_code ec;
	unsigned int length[1]; // const size array may be converted into asio buffer
	std::string data;

	//TODO serialze object
	data = msg->msg;

	// send msg header
	length[0] = (int)data.size();
	ec = _session.write(boost::asio::buffer(length));
	if (ec) return ec;

	// send msg
	ec = _session.write(boost::asio::buffer(&data[0], data.size()));
	
	return ec;
}

boost::system::error_code SenderLayer::readMsg(Msg* &msg)
{
	boost::system::error_code ec;
	unsigned int length[1]; // const size array may be converted into asio buffer
	std::string data;

	// read header
	ec = _session.read(boost::asio::buffer(length));
	if (ec) return ec;

	// prepare buffer, string length defined in bytes
	data.resize(length[0]);

	// read msg
	ec = _session.read(boost::asio::buffer(&data[0], data.size())); // c_str returns const char*

	//TODO deserialize msg
	msg = new Msg(data);
	
	return (ec);
}

boost::system::error_code SenderLayer::disconnect()
{
	return _session.stop();
}

ssl_tcp_socket::lowest_layer_type& SenderLayer::socket()
{
	return _session.lowestLayerSocket();
}