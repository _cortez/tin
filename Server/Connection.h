#pragma once

#include <boost/asio/io_service.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include "SenderLayer.h"
#include "Server.h"

class Server;

class Connection
{
public:
	Connection(Server* server, SenderLayer* senderLayer);
	void work();
	void stop();
private:
	void worker();

	boost::thread _workerThread;
	Server* _server;
	SenderLayer* _senderLayer;
};