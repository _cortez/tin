#pragma once

#include <boost/asio/io_service.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include "SSLSession.h"

struct Msg
{
	std::string msg;

	Msg(std::string& msg) : msg(msg) {};
};

class SenderLayer
{
public:
	SenderLayer(boost::asio::io_service& ioService, boost::asio::ssl::context& context, int ioTimeout);
	boost::system::error_code connect(boost::asio::ssl::stream_base::handshake_type type);
	boost::system::error_code sendMsg(const Msg* msg);
	boost::system::error_code readMsg(Msg* &msgReceived);
	boost::system::error_code disconnect();

	ssl_tcp_socket::lowest_layer_type& socket();
private:
	SSLSession _session;
};