#include "SSLSession.h"

SSLSession::SSLSession(boost::asio::io_service& ioService, boost::asio::ssl::context& context, int ioTimeout)
	: _socket(ioService, context),
	_ioDeadline(ioService),
	_ioTimeout(ioTimeout)
{
	_ioDeadline.expires_at(boost::posix_time::pos_infin);
	if (_ioTimeout != 0) _ioDeadline.async_wait(boost::bind(&SSLSession::deadlineStop, this, &_ioDeadline));
}

SSLSession::~SSLSession()
{
	stop();
}

ssl_tcp_socket::lowest_layer_type& SSLSession::lowestLayerSocket()
{
	return _socket.lowest_layer();
}

ssl_tcp_socket& SSLSession::sslSocket()
{
	return _socket;
}

boost::system::error_code SSLSession::sslHandshake(boost::asio::ssl::stream_base::handshake_type type)
{
	boost::system::error_code ec;
	_ioDeadline.expires_from_now(boost::posix_time::seconds(_ioTimeout));
	_socket.handshake(type, ec);
	_ioDeadline.expires_at(boost::posix_time::pos_infin);
	return ec; // return error instead of throwing exception
}

boost::system::error_code SSLSession::read(boost::asio::mutable_buffers_1& buffer)
{	
	boost::system::error_code ec;
	_ioDeadline.expires_from_now(boost::posix_time::seconds(_ioTimeout));
	boost::asio::read(_socket, buffer, ec);
	_ioDeadline.expires_at(boost::posix_time::pos_infin);
	return ec;
}

boost::system::error_code SSLSession::write(boost::asio::mutable_buffers_1& buffer)
{
	boost::system::error_code ec;
	_ioDeadline.expires_from_now(boost::posix_time::seconds(_ioTimeout));
	boost::asio::write(_socket, buffer, ec);
	_ioDeadline.expires_at(boost::posix_time::pos_infin);
	return ec;
}

bool SSLSession::isOpen()
{
	return lowestLayerSocket().is_open();
}

boost::system::error_code SSLSession::stop()
{
	boost::system::error_code ec;
	if (lowestLayerSocket().is_open())
	{
		_ioDeadline.expires_from_now(boost::posix_time::seconds(_ioTimeout));
		_socket.shutdown(ec); // try to safely close ssl
		//lowestLayerSocket().shutdown(boost::asio::socket_base::shutdown_type::shutdown_both, ec);
		lowestLayerSocket().close(); // enforce closing of tcp socket
		_ioDeadline.cancel();
	}
	return ec;
}

void SSLSession::deadlineStop(boost::asio::deadline_timer* timer)
{
	if (!isOpen()) return;

	if (timer->expires_at() <= boost::asio::deadline_timer::traits_type::now())
	{
		stop();
	}
	else if (_ioTimeout != 0)
	{
		timer->async_wait(boost::bind(&SSLSession::deadlineStop, this, timer));
	}
}