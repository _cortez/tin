#include "Connection.h"

Connection::Connection(Server* server, SenderLayer* senderLayer)
	: _server(server),
	_senderLayer(senderLayer)
{
}

void Connection::work()
{
	_workerThread = boost::thread(boost::bind(&Connection::worker, this));
}

void Connection::worker()
{
	boost::system::error_code ec;

	ec = _senderLayer->connect(boost::asio::ssl::stream_base::handshake_type::server);

	//TODO handle user id

	while (!ec)
	{
		Msg* msg = nullptr;

		ec = _senderLayer->readMsg(msg);
		if (ec) break;

		//TODO handle msg
		msg = _server->handleMsg(msg);

		ec = _senderLayer->sendMsg(msg);
	}

	_server->stopConnection(this);
}

void Connection::stop()
{
	_senderLayer->disconnect();
	_workerThread.join();
}