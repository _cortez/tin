#include "Server.h"

Server::Server(const Settings& settings)
	: _ioService(),
	// hton conversion automatically made by boost
	_acceptor(_ioService, 
		boost::asio::ip::tcp::endpoint(boost::asio::ip::address_v4::from_string(settings.ip), settings.port)), 
	_context(boost::asio::ssl::context::sslv23),
	_sslPass(settings.keyPass),
	_ioTimeout(settings.timeout)
{
	// Configure ssl
	_context.set_options(
		boost::asio::ssl::context::default_workarounds // Implement bugs workarounds
		| boost::asio::ssl::context::single_dh_use); // Negotiate new Diffie-Hellman key on each connect
	_context.set_password_callback(boost::bind(&Server::getSSLPassword, this));
	_context.use_certificate_chain_file(settings.cert);
	_context.use_private_key_file(settings.key, boost::asio::ssl::context::pem);
	_context.use_tmp_dh_file(settings.dhFile);

	// Mark a socket as accepting connections
	_acceptor.listen(settings.backlogSize);
}

void Server::startAccept()
{
	boost::system::error_code ec;
	SenderLayer* senderLayer = new SenderLayer(_ioService, _context, _ioTimeout);
	Connection* newConnection = new Connection(this, senderLayer);

	_acceptor.async_accept(senderLayer->socket(),
		boost::bind(&Server::handleAccept, this, newConnection, ec));

	_ioService.run();
}

void Server::handleAccept(Connection* newConnection, const boost::system::error_code& ec)
{
	if (!ec)
	{
		_activeConnections.insert(newConnection);
		newConnection->work();
	}
	else
	{
		delete newConnection;
	}

	if(_acceptor.is_open())startAccept();
}

void Server::stopConnection(Connection* connection)
{
	_ioService.post([this, connection]()
	{
		_activeConnections.erase(connection);
		delete connection;
	});
}

void Server::stop()
{
	_acceptor.close();
	for (Connection* c : _activeConnections) c->stop();
	_ioService.stop();
}

std::string Server::getSSLPassword()
{
	return _sslPass;
}

int Server::activeConnections()
{
	return (int)_activeConnections.size();
}

Msg* Server::handleMsg(Msg* msg)
{
	//TODO handle msg 
	return msg;
}