#pragma once

#include <set>
#include <boost\asio\io_service.hpp>
#include <boost\asio\ssl\context.hpp>
#include <boost\asio\ip\tcp.hpp>
#include <boost\bind.hpp>
#include "Connection.h"
#include "Settings.h"

class Connection;

class Server
{
public:
	Server(const Settings& settings);
	void startAccept();
	void stopConnection(Connection* connection);
	void stop();
	int activeConnections();
	Msg* handleMsg(Msg* msg);
private:
	void handleAccept(Connection* newConnection, const boost::system::error_code& ec);
	std::string getSSLPassword();

	boost::asio::io_service _ioService;
	boost::asio::ip::tcp::acceptor _acceptor;
	boost::asio::ssl::context _context;
	std::string _sslPass;
	std::set<Connection*> _activeConnections;
	int _ioTimeout;
};